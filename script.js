var inputVal = document.getElementById("textHere").value; //draw input for user as to what the subject matter of the photos will be


// var lat = [];
// var lon = [];
var lat;
var lon;
 var i = 0;

var picUrlOne;


var queuedPhotos = []; //queue of photos that will be displayed on page once coords are found

const delay = t => new Promise(resolve => setTimeout(resolve, t)); //https://stackoverflow.com/questions/39538473/using-settimeout-on-promise-chain


function isGeoLocationOn() {
    if (navigator.geolocation) { //if geolocation is supported on the user's browser, then execute
        navigator.geolocation.getCurrentPosition(success,error);
    } else { //if geolocation is not supported on the user's browser, then alert the user
        alert('Your browser will not allow geo-location accessability.');
}
}

function success(position) { //use geoLocation API to get user's current latitude and longitude, called in buildFlickrURL function
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    console.log(position.coords);
    setTimeout(buildFlickrURL,5700);
    // let pushLatitude = lat.push(position.coords.latitude); //assign your latitude coord to lat variable
    // let pushLongitude = lon.push(position.coords.longitude); 
}
  
function error() { //if an error is thrown, then alert that such an event took place
    alert('Sorry! We cannot find your current location.');
}
  
  

//isGeoLocationOn();
// function foo () {
//     console.log(lat);
// }
// function bar () {
//     console.log(lon);
// }
// setTimeout(foo,5000);
// setTimeout(bar,5000);


var picDiv = document.getElementById("imgID"); //create img element on webpage to display photos



function buildFlickrURL () { //prepend CORS proxy to Flickr API URL to build new Flickr URL
    //findMyLocation();
    var inputVal = document.getElementById("textHere").value;
    const proxy = "https://cors-anywhere.herokuapp.com/"; //set 'proxy' to CORS proxy
    let flickrURL = proxy + 'https://flickr.com/services/rest/?' + 
        'api_key=9ef002aed24cad88df03784bfd4631aa' + 
        '&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1' + 
        '&per_page=5&' + 
        'lat=' + lat + '&' + //include geelocation latitude
        'lon=' + lon + '&' +  //include geolocation longitude
        'text=' + inputVal; //include inputVal, specificied by user on webpage
    console.log(flickrURL);


   
    fetch(flickrURL)
        .then(responseObject => responseObject.json())
        .then(data => {
            console.log(data);
            data.photos.photo.forEach(element => { //for each of the 5 photos that are returned, execute code
            let convertedURL = photoURL(element); //use photoURL function to build the URL at which each picture is found 
            queuedPhotos.push(convertedURL); //push these new URLs to queuedPhotos, that user can scroll through using button
            //var picDiv = document.getElementById("imgID"); //create img element on webpage to display photos
            //var i = 0;
            picDiv.src = queuedPhotos[i]; //will allow an image, stored at one of the URL's in the queuedPhotos array, to be displayed in the 'img' div on the webpage once the URL's have been provided
            //console.log(queuedPhotos[0]);
        })
    });
    console.log(queuedPhotos[1]);
    // var picDiv = document.createElement("img"); //create img element on webpage to display photos
    // picDiv.src = queuedPhotos[1]; //will allow an image, stored at one of the URL's in the queuedPhotos array, to be displayed in the 'img' div on the webpage once the URL's have been provided
    // console.log(queuedPhotos[0]);
}





// fetch() will return a Promise (and you use Promise.prototype.then() on Promises).
// That Promise will provide a Response object to .then().
// That Response object has a method called .json().
// .json() will return a Promise.
// That Promise will provide the hydrated data to .then().


function photoURL (photoObj) { //function to construct the image source URL, provided on assessment page
    let finalProduct = "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
    return finalProduct;
    
}




document.addEventListener('keyup', grabPhotos); //add event listener to the 'Enter' key on keyword, allowing the user to run code with provided inputVal

function grabPhotos (event) {
    if (event.key == "Enter") {
        isGeoLocationOn();
        //buildFlickrURL();
    }

}

document.addEventListener('keyup', scrollPhotos); //add event listener to the 'ArrowRight'key on keyword, allowing the user to scroll through the array of photos provided by Flickr

function scrollPhotos (event) {
    if (event.key == "ArrowRight" && i < 4) {
        i += 1;
        console.log(queuedPhotos[i]);
        picDiv.src = queuedPhotos[i];
    }
    if (event.key == "ArrowRight" && picDiv.src == queuedPhotos[4]) {
        picDiv.src = queuedPhotos[0];
    }
}